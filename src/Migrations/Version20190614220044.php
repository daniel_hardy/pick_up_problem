<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190614220044 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE container_type (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE warehouse (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pickup (id INT AUTO_INCREMENT NOT NULL, customer_id INT NOT NULL, container_id INT DEFAULT NULL, haulier_id INT NOT NULL, warehouse_id INT NOT NULL, date DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_419E39FD9395C3F3 (customer_id), INDEX IDX_419E39FDBC21F742 (container_id), INDEX IDX_419E39FDE41BD2EA (haulier_id), INDEX IDX_419E39FD5080ECDE (warehouse_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE haulier (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE container (id INT AUTO_INCREMENT NOT NULL, container_type_id INT NOT NULL, number VARCHAR(255) DEFAULT NULL, weight INT DEFAULT NULL, INDEX IDX_C7A2EC1B5B3408DE (container_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE container ADD CONSTRAINT FK_C7A2EC1B5B3408DE FOREIGN KEY (container_type_id) REFERENCES container_type (id)');
        $this->addSql('ALTER TABLE pickup ADD CONSTRAINT FK_419E39FD9395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE pickup ADD CONSTRAINT FK_419E39FDBC21F742 FOREIGN KEY (container_id) REFERENCES container (id)');
        $this->addSql('ALTER TABLE pickup ADD CONSTRAINT FK_419E39FDE41BD2EA FOREIGN KEY (haulier_id) REFERENCES haulier (id)');
        $this->addSql('ALTER TABLE pickup ADD CONSTRAINT FK_419E39FD5080ECDE FOREIGN KEY (warehouse_id) REFERENCES warehouse (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE container DROP FOREIGN KEY FK_C7A2EC1B5B3408DE');
        $this->addSql('ALTER TABLE pickup DROP FOREIGN KEY FK_419E39FD5080ECDE');
        $this->addSql('ALTER TABLE pickup DROP FOREIGN KEY FK_419E39FDE41BD2EA');
        $this->addSql('ALTER TABLE pickup DROP FOREIGN KEY FK_419E39FD9395C3F3');
        $this->addSql('ALTER TABLE pickup DROP FOREIGN KEY FK_419E39FDBC21F742');
        $this->addSql('DROP TABLE IF EXISTS container_type');
        $this->addSql('DROP TABLE IF EXISTS warehouse');
        $this->addSql('DROP TABLE IF EXISTS pickup');
        $this->addSql('DROP TABLE IF EXISTS haulier');
        $this->addSql('DROP TABLE IF EXISTS customer');
        $this->addSql('DROP TABLE IF EXISTS container');
    }
}

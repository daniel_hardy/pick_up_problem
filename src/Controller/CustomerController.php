<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Util\Response;
use App\Util\Serializer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CustomerController extends AbstractController
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    protected $repository;

    /**
     * @var \App\Util\Serializer
     */
    protected $serializer;

    /**
     * @var \App\Util\Response
     */
    protected $response;

    /**
     * CustomerController constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Customer::class);
        $this->serializer = new Serializer();
        $this->response = new Response();
    }

    /**
     * Get all customers.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function all()
    {
        $results = $this->serializer->json(
            ['customers' => $this->repository->findAll()]
        );

        return $this->response->json($results);
    }
}

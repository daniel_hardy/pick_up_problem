<?php

namespace App\Controller;

use App\Util\Response;

class DefaultController
{
    /**
     * @var \App\Util\Response
     */
    protected $response;

    /**
     * DefaultController constructor.
     */
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * Default route of website.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->response->html(
            '<html><body>Welcome to the api</body></html>'
        );
    }
}

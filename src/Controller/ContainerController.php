<?php

namespace App\Controller;

use App\Entity\Container;
use App\Util\Response;
use App\Util\Serializer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContainerController extends AbstractController
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    protected $repository;

    /**
     * @var \App\Util\Serializer
     */
    protected $serializer;

    /**
     * @var \App\Util\Response
     */
    protected $response;

    /**
     * ContainerController constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Container::class);
        $this->serializer = new Serializer();
        $this->response = new Response();
    }

    /**
     * Get all containers.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function all()
    {
        $results = $this->serializer->json(
            ['containers' => $this->repository->findAll()]
        );

        return $this->response->json($results);
    }
}

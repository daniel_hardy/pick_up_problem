<?php

namespace App\Controller;

use App\Entity\Haulier;
use App\Util\Response;
use App\Util\Serializer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HaulierController extends AbstractController
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    protected $repository;

    /**
     * @var \App\Util\Serializer
     */
    protected $serializer;

    /**
     * @var \App\Util\Response
     */
    protected $response;

    /**
     * HaulierController constructor.
     *
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Haulier::class);
        $this->serializer = new Serializer();
        $this->response = new Response();
    }

    /**
     * Get all hauliers.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function all()
    {
        $results = $this->serializer->json(
            ['hauliers' => $this->repository->findAll()]
        );

        return $this->response->json($results);
    }
}

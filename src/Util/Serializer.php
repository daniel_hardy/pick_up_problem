<?php

namespace App\Util;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as SymfonySerializer;

class Serializer
{
    /**
     * @var \Symfony\Component\Serializer\Serializer
     */
    protected $serializer;

    /**
     * Serializer constructor.
     */
    public function __construct()
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new SymfonySerializer($normalizers, $encoders);
    }

    /**
     * Serialize an array of data into json.
     *
     * @param array $results
     * @param array $ignoreAttributes
     *
     * @return bool|float|int|string
     */
    public function json(array $results = [], array $ignoreAttributes = [])
    {
        $ignoreAttributes = array_merge($ignoreAttributes, [
            '__initializer__',
            '__cloner__',
            '__isInitialized__',
        ]);

        return $this->serializer->serialize(
            $results,
            'json',
            [
                'ignored_attributes' => $ignoreAttributes,
            ]
        );
    }

    /**
     * Serialize an array of data into xml.
     *
     * @param array $results
     *
     * @return bool|float|int|string
     */
    public function xml(array $results = [])
    {
        return $this->serializer->serialize($results, 'xml');
    }
}

<?php

namespace App\Util;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class Response
{
    /**
     * Respond as json.
     *
     * @param string $body
     *
     * @return mixed
     */
    public function json(string $body = '')
    {
        $response = new JsonResponse();

        return $response->setJson($body);
    }

    /**
     * Respond as html.
     *
     * @param string $body
     *
     * @return mixed
     */
    public function html(string $body = '')
    {
        $response = new SymfonyResponse();

        return $response->setContent($body);
    }
}

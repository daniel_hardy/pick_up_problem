<?php

namespace App\DataFixtures;

use App\Entity\Container;
use App\Entity\ContainerType;
use App\Entity\Customer;
use App\Entity\Haulier;
use App\Entity\Pickup;
use App\Entity\Warehouse;
use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // add a customer
        $customer = new Customer();
        $customer->setName('GOT Enterprises');
        $manager->persist($customer);

        // add a warehouse
        $warehouse = new Warehouse();
        $warehouse->setName('Red Keep');
        $manager->persist($warehouse);

        // add a haulier
        $haulier = new Haulier();
        $haulier->setName('Targaryen');
        $manager->persist($haulier);

        // add a container type
        $containerType = new ContainerType();
        $containerType->setCode('22GP');
        $containerType->setDescription('20 General Purpose');
        $manager->persist($containerType);

        // add a container
        $container = new Container();
        $container->setNumber('MSCU1234566');
        $container->setWeight(150);
        $container->setContainerType($containerType);
        $manager->persist($container);

        // add a pickup (and all associated entities created above)
        $carbon = Carbon::parse('2019-01-01 00:00:00');
        $pickup = new Pickup();
        $pickup->setContainer($container);
        $pickup->setCustomer($customer);
        $pickup->setCreatedAt($carbon);
        $pickup->setDate($carbon);
        $pickup->setHaulier($haulier);
        $pickup->setUpdateAt($carbon);
        $pickup->setWarehouse($warehouse);
        $manager->persist($pickup);

        $manager->flush();
    }
}

<?php

namespace App\Tests\Util;

use App\Util\Response;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

class ResponseTest extends TestCase
{
    /**
     * @var \App\Util\Response;
     */
    protected $response;
    /**
     * ResponseTest constructor.
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->response = new Response();
    }

    /** @test */
    public function can_respond_in_json_format()
    {
        $response = $this->response->json("{'pickups':[]}");

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals("{'pickups':[]}", $response->getContent());
        $this->assertEquals("application/json", $response->headers->get('Content-Type'));
    }

    /** @test */
    public function can_respond_in_html_format()
    {
        $response = $this->response->html('<h1>hello</h1>');

        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\Response::class, $response);
        $this->assertEquals('<h1>hello</h1>', $response->getContent());
    }
}
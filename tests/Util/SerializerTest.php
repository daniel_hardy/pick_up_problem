<?php

namespace App\Tests\Util;

use App\Util\Serializer;
use PHPUnit\Framework\TestCase;

class SerializerTest extends TestCase
{
    /**
     * @var \App\Util\Serializer;
     */
    protected $serializer;

    /**
     * SerializerTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->serializer = new Serializer();
    }

    /** @test */
    public function can_serialized_array_to_json()
    {
        $input = [
            'customers' => [
                [
                    'id' => 1,
                    'name' => 'GOT Enterprises'
                ]
            ]
        ];
        $expected = json_encode($input);

        $this->assertEquals($expected, $this->serializer->json($input));
    }
}
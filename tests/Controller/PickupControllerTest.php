<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PickupControllerTest extends WebTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $url = 'api/pickups';

    /**
     * PickupControllerTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->httpClient = static::createClient();
    }

    /** @test */
    public function response_is_ok()
    {
        $this->httpClient->request('GET', $this->url);

        $this->assertEquals(
            200,
            $this->httpClient->getResponse()->getStatusCode()
        );
    }

    /** @test */
    public function can_get_pickups()
    {
        $this->httpClient->request('GET', $this->url);
        $json = $this->httpClient->getResponse()->getContent();
        $response = json_decode($json, true);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('pickups', $response);
        $this->assertArrayHasKey('id', $response['pickups'][0]);
        $this->assertArrayHasKey('date', $response['pickups'][0]);
        $this->assertArrayHasKey('createdAt', $response['pickups'][0]);
        $this->assertArrayHasKey('updatedAt', $response['pickups'][0]);
        $this->assertArrayHasKey('customer', $response['pickups'][0]);
        $this->assertArrayHasKey('container', $response['pickups'][0]);
        $this->assertArrayHasKey('warehouse', $response['pickups'][0]);
        $this->assertArrayHasKey('haulier', $response['pickups'][0]);
    }
}
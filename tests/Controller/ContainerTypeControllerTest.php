<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContainerTypeControllerTest extends WebTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $url = 'api/containertypes';

    /**
     * ContainerTypeControllerTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->httpClient = static::createClient();
    }

    /** @test */
    public function response_is_ok()
    {
        $this->httpClient->request('GET', $this->url);

        $this->assertEquals(
            200,
            $this->httpClient->getResponse()->getStatusCode()
        );
    }

    /** @test */
    public function can_get_container_types()
    {
        $this->httpClient->request('GET', $this->url);
        $json = $this->httpClient->getResponse()->getContent();
        $response = json_decode($json, true);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('containertypes', $response);
        $this->assertArrayHasKey('id', $response['containertypes'][0]);
        $this->assertArrayHasKey('code', $response['containertypes'][0]);
        $this->assertArrayHasKey('description', $response['containertypes'][0]);
    }
}
<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WarehouseControllerTest extends WebTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $url = 'api/warehouses';

    /**
     * WarehouseControllerTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->httpClient = static::createClient();
    }

    /** @test */
    public function response_is_ok()
    {
        $this->httpClient->request('GET', $this->url);

        $this->assertEquals(
            200,
            $this->httpClient->getResponse()->getStatusCode()
        );
    }

    /** @test */
    public function can_get_warehouses()
    {
        $this->httpClient->request('GET', $this->url);
        $json = $this->httpClient->getResponse()->getContent();
        $response = json_decode($json, true);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('warehouses', $response);
        $this->assertArrayHasKey('id', $response['warehouses'][0]);
        $this->assertArrayHasKey('name', $response['warehouses'][0]);
    }
}
<?php

namespace App\Tests\Entity;

use App\Entity\ContainerType;
use PHPUnit\Framework\TestCase;

class ContainerTypeTest extends TestCase
{
    /** @test */
    public function can_get_and_set_properties()
    {
        $containerType = new ContainerType();
        $containerType->setCode('22BU');
        $containerType->setDescription('20 BULK CONTAINER');

        $this->assertEquals('22BU', $containerType->getCode());
        $this->assertEquals('20 BULK CONTAINER', $containerType->getDescription());
    }
}
<?php

namespace App\Tests\Entity;

use App\Entity\Warehouse;
use PHPUnit\Framework\TestCase;

class WarehouseTest extends TestCase
{
    /** @test */
    public function can_get_and_set_properties()
    {
        $warehouse = new Warehouse();
        $warehouse->setName('Red Keep');

        $this->assertEquals('Red Keep', $warehouse->getName());
    }
}
<?php

namespace App\Tests\Entity;

use App\Entity\Pickup;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class PickupTest extends TestCase
{
    /** @test */
    public function can_get_and_set_properties()
    {
        $expected = $input = '2019-06-14 10:00:00';
        $pickup = new Pickup();
        $pickup->setCreatedAt(Carbon::parse($input));
        $pickup->setUpdateAt(Carbon::parse($input));

        $this->assertEquals($expected, $pickup->getCreatedAt());
        $this->assertEquals($expected, $pickup->getUpdatedAt());
    }
}
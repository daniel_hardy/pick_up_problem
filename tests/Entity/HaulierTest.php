<?php

namespace App\Tests\Entity;

use App\Entity\Haulier;
use PHPUnit\Framework\TestCase;

class HaulierTest extends TestCase
{
    /** @test */
    public function can_get_and_set_properties()
    {
        $haulier = new Haulier();
        $haulier->setName('Stark');

        $this->assertEquals('Stark', $haulier->getName());
    }
}
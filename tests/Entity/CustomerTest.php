<?php

namespace App\Tests\Entity;

use App\Entity\Customer;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    /** @test */
    public function can_get_and_set_properties()
    {
        $customer = new Customer();
        $customer->setName('GOT Enterprises');

        $this->assertEquals('GOT Enterprises', $customer->getName());
    }
}
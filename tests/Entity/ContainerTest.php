<?php

namespace App\Tests\Entity;

use App\Entity\Container;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    /** @test */
    public function can_get_and_set_properties()
    {
        $container = new Container();
        $container->setNumber('123ASD1223');
        $container->setWeight('1400');

        $this->assertEquals('123ASD1223', $container->getNumber());
        $this->assertEquals('1400', $container->getWeight());
    }
}
# Pickup Problem

A customer has asked you to build a simple warehouse system as a REST API to manage containers that are picked up by hauliers from their warehouse for UK Export.

The customer wants to allow nominated freight forwarders access to the REST API to provide haulier, container type and pickup date in advance while warehouse staff will update the container number and weight after pickup has occurred.

### How do I get set up? ###

1. Clone repo;
* git clone https://bitbucket.org/daniel_hardy/pick_up_problem/src/master/

2. Install dependancies for the project;
* composer install

3. Run the migrations
* php bin/console doctrine:migrations:migrate

4. Run the seeds
* php bin/console doctrine:fixtures:load 
   
5. Run the application
* symfony serve
   
6. Visit url 
* http://127.0.0.1:8000/
      
7. Run the tests
* ./vendor/bin/phpunit